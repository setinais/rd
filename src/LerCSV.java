import jdk.jshell.execution.Util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;

/**
 *
 */
public class LerCSV {

    public ArrayList<Double[][]> obterDados(int nEntrada, int nSaida, String file) throws IOException {

        BufferedReader br = null;
        String linha = "";
        String csvDivisor = ",";
        ArrayList<double[]> dados = new ArrayList<>();
        try {

            br = new BufferedReader(new FileReader(file));
            br.readLine();
            while ((linha = br.readLine()) != null) {

                String[] dadosLinha = linha.split(csvDivisor);
                if(dadosLinha.length == 0 )
                    continue;
                double[] data = new double[dadosLinha.length];
                for(int s = 0; s< dadosLinha.length; s++){
                        data[s] = Double.parseDouble(dadosLinha[s]);
                }
                dados.add(data);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for(int o=0;o<=10;o++){
            System.out.println("L "+(o)+" - "+dados.get(o)[0]);
        }
//        BufferedImage imagemSoma = new BufferedImage(28, 28, BufferedImage.TYPE_INT_RGB);
//        int contar = 1;
//        for (int q=0;q < 28;q++){
//            for(int e = 0; e < 28; e++){
//                if(dados.get(20)[contar] < 1){
//                    imagemSoma.setRGB(q,e,255);
//                }else{
//                    imagemSoma.setRGB(q,e,0);
//                }
//
//                contar++;
//            }
//        }
//        ImageIO.write(imagemSoma, "jpg", new File("imagemSoma2.jpg"));
        return converterArrayParaDouble(dados , nEntrada, nSaida);
    }


    private ArrayList<Double[][]> converterArrayParaDouble(ArrayList<double[]> dados, int nentrada, int nsaida) throws IOException {
        int entrada = nentrada;
        int saida = nsaida;
        ArrayList<Double[][]> retorno = new ArrayList<Double[][]>();
        Double[][] matrizEntradas = new Double[dados.size()][entrada];
        Double[][] matrizResultados = new Double[dados.size()][saida];
//        FileWriter arq = new FileWriter("representation.txt");
//        PrintWriter gravarArq = new PrintWriter(arq);
//        gravarArq.printf("+--Resultado--+%n");
        for (int i = 0; i < dados.size(); i++) {

            for (int j = 0; j < (entrada+1); j++) {
                if (j < 1) {
                    matrizResultados[i] = outputNumber((int) dados.get(i)[j]);
//                    gravarArq.printf("%nNumero: "+((int) dados.get(i)[j])+"  Representation: ");
//                    for(int q=0;q<matrizResultados[i].length;q++) {
//                        gravarArq.print( matrizResultados[i][q].intValue()+",");
//                    }
                } else {
                    if(dados.get(i)[j] != 0.0) {
                        matrizEntradas[i][j - 1] = 1.0;
                    }else {
                        matrizEntradas[i][j - 1] = 0.0;
                    }
                }
            }
        }

        //arq.close();
        retorno.add(matrizEntradas);
        retorno.add(matrizResultados);
        return retorno;
    }

    public Double[] outputNumber(int number){
        String[] representation = {""};
        switch (number){
            case 9:
                String dados = "0,0,0,0,0,0,0,0,0,1";
                representation = dados.split(",");
                break;
            case 8:
                String dados1 = "0,0,0,0,0,0,0,0,1,0";
                representation = dados1.split(",");
                break;
            case 7:
                String dados2 = "0,0,0,0,0,0,0,1,0,0";
                representation = dados2.split(",");
                break;
            case 6:
                String dados3 = "0,0,0,0,0,0,1,0,0,0";
                representation = dados3.split(",");
                break;
            case 5:
                String dados4 = "0,0,0,0,0,1,0,0,0,0";
                representation = dados4.split(",");
                break;
            case 4:
                String dados5 = "0,0,0,0,1,0,0,0,0,0";
                representation = dados5.split(",");
                break;
            case 3:
                String dados6 = "0,0,0,1,0,0,0,0,0,0";
                representation = dados6.split(",");
                break;
            case 2:
                String dados7 = "0,0,1,0,0,0,0,0,0,0";
                representation = dados7.split(",");
                break;
            case 1:
                String dados8 = "0,1,0,0,0,0,0,0,0,0";
                representation = dados8.split(",");
                break;
            case 0:
                String dados9 = "1,0,0,0,0,0,0,0,0,0";
                representation = dados9.split(",");
                break;
        }
        return converter(representation);
    }

    public Double[] converter(String[] representation){
        Double[] convesion = new Double[10];
        for(int i=0;i<10;i++){
            convesion[i] = Double.parseDouble(representation[i]);
        }
        return convesion;
    }

}

