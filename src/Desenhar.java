import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;

public class Desenhar extends JPanel {

    public BufferedImage bufferedImage;

    public Desenhar(){
        super(true);
        int width = 588;
        int height = 588;

        bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);// cria um buffer com o tamanho e o tipo definido, outro tipo comum BufferedImage.TYPE_INT_RGB
        addMouseListener(new AcaoMouse());//acao de clike
        addMouseMotionListener(new AcaoMouse());//acao de arrastar (drag)
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.WHITE); //seleciona cor de fundo
        g.fillRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());//pinta tudo com a cor selecionada
        g.drawImage(bufferedImage, 0, 0, null); // pinta o jpanel com o buffer
        g.dispose();
    }

    public void limpa(){
        bufferedImage.getGraphics().setColor(Color.WHITE);
        bufferedImage.getGraphics().fillRect(0,0,588,588);
        bufferedImage.getGraphics().drawImage(bufferedImage, 0,0,null);
        bufferedImage.getGraphics().dispose();
        repaint();
    }

    private class AcaoMouse implements MouseListener, MouseMotionListener {
        public void mouseClicked(MouseEvent e) {
        }

        public void mousePressed(MouseEvent e) {
            pinta(e.getX(), e.getY());
        }

        public void mouseReleased(MouseEvent e) {
            pinta(e.getX(), e.getY());
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        public void mouseDragged(MouseEvent e) {
            pinta(e.getX(), e.getY());
        }

        public void mouseMoved(MouseEvent e) {
        }

        private void pinta(int x, int y) {
            Graphics gDoBuffer = bufferedImage.createGraphics();//pega o graphics do buffer para edicao
            gDoBuffer.setColor(Color.BLACK); //seta a cor do pincel
            gDoBuffer.fillRect(x, y, 1, 1); //desenha um ponto
            gDoBuffer.fillRect(x+1, y, 1, 1);
            gDoBuffer.fillRect(x+1, y+1, 1, 1);
            gDoBuffer.fillRect(x-1, y+1, 1, 1);
            gDoBuffer.fillRect(x-1, y-1, 1, 1);
            gDoBuffer.fillRect(x-1, y, 1, 1);
            gDoBuffer.fillRect(x, y-1, 1, 1);
            gDoBuffer.fillRect(x, y+1, 1, 1);
            gDoBuffer.fillRect(x+1, y-1, 1, 1);
            gDoBuffer.fillRect(x-2, y-2, 1, 1);
            gDoBuffer.fillRect(x-1, y-2, 1, 1);
            gDoBuffer.fillRect(x, y-2, 1, 1);
            gDoBuffer.fillRect(x+1, y-2, 1, 1);
            gDoBuffer.fillRect(x+2, y-2, 1, 1);
            gDoBuffer.fillRect(x-2, y+2, 1, 1);
            gDoBuffer.fillRect(x-1, y+2, 1, 1);
            gDoBuffer.fillRect(x, y+2, 1, 1);
            gDoBuffer.fillRect(x+1, y+2, 1, 1);
            gDoBuffer.fillRect(x+2, y+2, 1, 1);
            gDoBuffer.fillRect(x-2, y-1, 1, 1);
            gDoBuffer.fillRect(x-2, y, 1, 1);
            gDoBuffer.fillRect(x-2, y+1, 1, 1);
            gDoBuffer.fillRect(x+2, y-1, 1, 1);
            gDoBuffer.fillRect(x+2, y, 1, 1);
            gDoBuffer.fillRect(x+2, y+1, 1, 1);
            gDoBuffer.dispose();
            updateUI();//atualiza o jpanel, ou seja, "diz ao jpanel q seu desenho foi atualizado e vc qé q seja exibido"
        }
    }

    //Redimensiona a imagem para o tamanho desejado
    public BufferedImage usandoGetScaled(BufferedImage img, int larguraFinal, int alturaFinal, int modo) {

        Image tmp = img.getScaledInstance(larguraFinal, alturaFinal, modo);
        int width = tmp.getWidth(null);
        int height = tmp.getHeight(null);

        BufferedImage novaImagem = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_ARGB);
        Graphics g = novaImagem.createGraphics();
        g.drawImage(tmp, 0, 0, null);
        g.dispose();

        return novaImagem;

    }

    public BufferedImage normalizarQuadro(BufferedImage img, int lagf, int altf, int modo){
        Image tmp = img.getScaledInstance(lagf,altf, modo);
        BufferedImage novaimagem = new BufferedImage(lagf, altf, BufferedImage.TYPE_INT_ARGB);
        Graphics g = novaimagem.createGraphics();
        g.fillRect(0,0,lagf,altf);
        g.dispose();
        return novaimagem;
    }

    public void setImageTest(BufferedImage img){
        bufferedImage = img;
        repaint();
    }
}
